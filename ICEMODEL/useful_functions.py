'''Miscellaneous useful functions.'''
import numpy as np
import copy
from scipy import optimize
import cantera as ct

from ICEMODEL.zone_defs import zone


def get_mc_volume(theta, stroke, conrod, bore, V_TDC, V_pc):
    """Compute the main chamber volume from crank position."""
    hrs = 1./2.*stroke*np.sin(theta)
    sqr = np.sqrt(conrod**2 - hrs**2)
    dell = 1./2.*stroke*(1. - np.cos(theta)) + conrod - sqr
    vol = V_TDC - V_pc + dell*np.pi*bore**2./4.
    return vol


def get_zpiston(theta, stroke, conrod, bore):
    """Compute the distance to TDC from crank position."""
    hrs = 1./2.*stroke*np.sin(theta)
    sqr = np.sqrt(conrod**2 - hrs**2)
    dell = 1./2.*stroke*(1. - np.cos(theta)) + conrod - sqr
    return dell


def init_states(species_names, N, Nd):
    '''Build the states dict.'''
    states = {}

    # CAD
    states['CAD'] = np.zeros(N)

    # DUCT STATES
    states['duct'] = {}
    for side in ['pcside', 'mcside']:
        states['duct'][side] = {}
        for var in ['U', 'T']:
            states['duct'][side][var] = np.zeros([Nd, N])
    states['duct']['mflow'] = np.zeros([Nd, N])
    states['duct']['TJI'] = np.zeros([Nd, N])

    # ZONE STATES
    zoneList = ['mc', 'mcf', 'mcb', 'pc', 'pcf', 'pcb']
    varList = ['temperature', 'pressure', 'mass', 'volume', 'phi',
               'flame_surface', 'mk', 'mflow_in', 'mflow_out']
    for spec in species_names:
        varList.append(spec)
    for zone in zoneList:
        states[zone] = {}
        for var in varList:
            states[zone][var] = np.zeros(N)

    return states


def crop_states(states, species_names, idx):
    '''Crop states'''

    # crank angle
    states['CAD'] = states['CAD'][:idx]

    # duct
    for side in ['pcside', 'mcside']:
        for var in ['U', 'T']:
            states['duct'][side][var] = states['duct'][side][var][:, :idx]
    states['duct']['mflow'] = states['duct']['mflow'][:, :idx]
    states['duct']['TJI'] = states['duct']['TJI'][:, :idx]

    # zone
    zoneList = ['mc', 'mcf', 'mcb', 'pc', 'pcf', 'pcb']
    varList = ['temperature', 'pressure', 'mass', 'volume', 'phi',
               'flame_surface', 'mk', 'mflow_in', 'mflow_out']
    for spec in species_names:
        varList.append(spec)
    for zone in zoneList:
        for var in varList:
            states[zone][var] = states[zone][var][:idx]

    return states


def update_states(states, mc, mcf, mcb, pc, pcf, pcb, duct, CAD, it):
    '''
    Add the current step state to the dict of states.
    '''
    # crank angle
    states['CAD'][it] = CAD

    # duct properties
    for i in range(len(duct.d)):
        if pc.gas.P >= mc.gas.P:
            states['duct']['pcside']['U'][i, it] = duct.Uin[i]
            states['duct']['mcside']['U'][i, it] = duct.Uout[i]
            states['duct']['pcside']['T'][i, it] = duct.Tin[i]
            states['duct']['mcside']['T'][i, it] = duct.Tout[i]
        else:
            states['duct']['mcside']['U'][i, it] = duct.Uin[i]
            states['duct']['pcside']['U'][i, it] = duct.Uout[i]
            states['duct']['mcside']['T'][i, it] = duct.Tin[i]
            states['duct']['pcside']['T'][i, it] = duct.Tout[i]
        states['duct']['mflow'][i, it] = duct.mflow[i]
        states['duct']['TJI'][i, it] = duct.TJI[i]

    # zone properties
    for zone in [mc, mcf, mcb, pc, pcf, pcb]:
        states[zone.name]['temperature'][it] = zone.gas.T
        states[zone.name]['pressure'][it] = zone.gas.P
        states[zone.name]['mass'][it] = zone.mass
        states[zone.name]['volume'][it] = zone.volume
        states[zone.name]['phi'][it] = zone.phi
        states[zone.name]['flame_surface'][it] = zone.FlamSurf
        states[zone.name]['mk'][it] = zone.mk
        states[zone.name]['mflow_in'][it] = np.sum(zone.mflow_in)
        states[zone.name]['mflow_out'][it] = np.sum(zone.mflow_out)
        for k, spec in enumerate(zone.gas.species_names):
            states[zone.name][spec][it] = zone.gas.Y[k]

    return states


def compute_initial_yvect(gas, cti, opDefs, engineDefs, ode, sl_table):
    '''
    Initialize temporary zones: mc, mcf, mcb, pc, pcf, pcb
    then compute yvect for the ode solver.
    '''

    # easier var names
    vid = ode.varIdx
    nvar = ode.nvar
    nsp = gas.n_species

    # initial main chamber volume
    Vmc0 = get_mc_volume(opDefs.RFA*np.pi/180., engineDefs.stroke,
                         engineDefs.conrod, engineDefs.bore, engineDefs.V_TDC,
                         engineDefs.V_pc)

    #####################################
    # initial main chamber zones
    #####################################

    # -> initialize the main chamber burnt gases zone with small quantity
    # of burnt gases
    # zone defs
    mc = zone(cti, 'mc', sl_table)
    mcf = zone(cti, 'mcf', sl_table)
    mcb = zone(cti, 'mcb', sl_table)
    # known variables
    mcf.gas.set_equivalence_ratio(opDefs.eqratio_intake,
                                  'C3H8:1', 'O2:1, N2:3.76')
    specarr = np.zeros_like(mcb.gas.Y)
    specarr[mcb.gas.species_index('CO2')] = opDefs.residuals_mc_Yk['CO2']
    specarr[mcb.gas.species_index('H2O')] = opDefs.residuals_mc_Yk['H2O']
    specarr[mcb.gas.species_index('N2')] = opDefs.residuals_mc_Yk['N2']
    mcb.gas.Y = copy.deepcopy(specarr)
    mc.volume = Vmc0
    mc.gas.TPY = (opDefs.T_mc_RFA, opDefs.P_mc_RFA,
                  (mcb.gas.Y*opDefs.residuals_mc_mass_fraction_RFA
                   + (1.-opDefs.residuals_mc_mass_fraction_RFA)*mcf.gas.Y))
    # compute the rest
    mc.mass = mc.gas.density*mc.volume
    mcb.mass = mc.mass*opDefs.residuals_mc_mass_fraction_RFA
    mcf.mass = mc.mass*(1.-opDefs.residuals_mc_mass_fraction_RFA)

    # (scipy root solver to find the system solution giving Umc = Umcf + Umcb)
    # func def
    def fmc(mcf_tempe, opDefs, mc, mcb, mcf):
        mcf.gas.TPY = mcf_tempe, opDefs.P_mc_RFA, mcf.gas.Y
        # ideal gas law
        mcf.volume = (mcf.mass/mc.gas.P*ct.gas_constant
                      /mcf.gas.mean_molecular_weight*mcf.gas.T)
        # volume law
        mcb.volume = mc.volume - mcf.volume
        # ideal gas law
        Tmcb = (mc.gas.P*mcb.volume/mcb.mass*mcb.gas.mean_molecular_weight
                /ct.gas_constant)
        mcb.gas.TPY = Tmcb, mc.gas.P, mcb.gas.Y
        # mc total int e must be equal to the sum of mcf and mcb total int e
        objective_func = (mc.gas.int_energy_mass*mc.mass
                          - (mcf.gas.int_energy_mass*mcf.mass
                             + mcb.gas.int_energy_mass*mcb.mass))
        return objective_func
    # initial guess
    Tguess = opDefs.T_mc_RFA
    # root solver
    # sol = optimize.root(fmc, Tguess, args=(opDefs, mc, mcb, mcf),
    #                     method='krylov', options={'fatol':1e-30,
    #                     'maxiter':500})
    sol = optimize.root(fmc, Tguess, args=(opDefs, mc, mcb, mcf),
                        method='hybr')
    if not sol.success:
        sys.exit(' ERROR: scipy optmize root solver did not converge \
                 (mc zone init).')
    Tsol = sol.x
    # Tsol is now the mcf_tempe
    mcf.gas.TPY = Tsol, opDefs.P_mc_RFA, mcf.gas.Y
    # ideal gas law
    mcf.volume = (mcf.mass/mc.gas.P*ct.gas_constant
                  /mcf.gas.mean_molecular_weight*mcf.gas.T)
    # volume law
    mcb.volume = mc.volume - mcf.volume
    # ideal gas law
    Tmcb = (mc.gas.P*mcb.volume/mcb.mass*mcb.gas.mean_molecular_weight
            /ct.gas_constant)
    mcb.gas.TPY = Tmcb, mc.gas.P, mcb.gas.Y

    #####################################
    # initial pre chamber zones
    #####################################

    # zone defs
    pc = zone(cti, 'pc', sl_table)
    pcf = zone(cti, 'pcf', sl_table)
    pcb = zone(cti, 'pcb', sl_table)
    # known variables
    pcf.gas.set_equivalence_ratio(opDefs.eqratio_intake,
                                  'C3H8:1', 'O2:1, N2:3.76')
    specarr = np.zeros_like(pcb.gas.Y)
    specarr[pcb.gas.species_index('CO2')] = opDefs.residuals_pc_Yk['CO2']
    specarr[pcb.gas.species_index('H2O')] = opDefs.residuals_pc_Yk['H2O']
    specarr[pcb.gas.species_index('N2')] = opDefs.residuals_pc_Yk['N2']
    pcb.gas.Y = copy.deepcopy(specarr)
    pc.volume = engineDefs.V_pc
    pc.gas.TPY = (opDefs.T_pc_RFA, opDefs.P_pc_RFA,
                  (pcb.gas.Y*opDefs.residuals_pc_mass_fraction_RFA
                   + (1. - opDefs.residuals_pc_mass_fraction_RFA)*pcf.gas.Y))
    # compute the rest
    pc.mass = pc.gas.density*pc.volume
    pcb.mass = pc.mass*opDefs.residuals_pc_mass_fraction_RFA
    pcf.mass = pc.mass*(1.-opDefs.residuals_pc_mass_fraction_RFA)

    # (scipy root solver to find the system solution giving Upc = Upcf + Upcb)
    # func def
    def fpc(pcf_tempe, opDefs, pc, pcb, pcf):
        pcf.gas.TPY = pcf_tempe, opDefs.P_pc_RFA, pcf.gas.Y
        # ideal gas law
        pcf.volume = (pcf.mass/pc.gas.P*ct.gas_constant
                      /pcf.gas.mean_molecular_weight*pcf.gas.T)
        # volume law
        pcb.volume = pc.volume - pcf.volume
        # ideal gas law
        Tpcb = (pc.gas.P*pcb.volume/pcb.mass*pcb.gas.mean_molecular_weight
                /ct.gas_constant)
        pcb.gas.TPY = Tpcb, pc.gas.P, pcb.gas.Y
        # pc total int e must be equal to the sum of pcf and pcb total int e
        objective_func = (pc.gas.int_energy_mass*pc.mass
                          - (pcf.gas.int_energy_mass*pcf.mass
                             + pcb.gas.int_energy_mass*pcb.mass))
        return objective_func
    # initial guess
    Tguess = opDefs.T_mc_RFA
    # root solver
    # sol = optimize.root(fpc, Tguess, args=(opDefs, pc, pcb, pcf),
    #                     method='krylov', options={'fatol':1e-30,
    #                     'maxiter':500})
    sol = optimize.root(fpc, Tguess, args=(opDefs, pc, pcb, pcf),
                        method='hybr')
    if not sol.success:
        sys.exit(' ERROR: scipy optmize root solver did not converge \
                 (pc zone init).')
    Tsol = sol.x
    # Tsol is now the pcf_tempe
    pcf.gas.TPY = Tsol, opDefs.P_pc_RFA, pcf.gas.Y
    # ideal gas law
    pcf.volume = (pcf.mass/pc.gas.P*ct.gas_constant
                  /pcf.gas.mean_molecular_weight*pcf.gas.T)
    # volume law
    pcb.volume = pc.volume - pcf.volume
    # ideal gas law
    Tpcb = (pc.gas.P*pcb.volume/pcb.mass*pcb.gas.mean_molecular_weight
            /ct.gas_constant)
    pcb.gas.TPY = Tpcb, pc.gas.P, pcb.gas.Y

    # -> fill y vector of variables at initial state
    y = np.zeros(nvar, 'd')
    i = 0
    # mc
    y[vid['Vmc']] = mc.volume
    i += 1
    y[vid['Pmc']] = mc.gas.P
    i += 1
    y[vid['FlamSurfmc']] = 0.
    i += 1
    y[vid['mkmc']] = 0.
    i += 1
    # mcf
    y[vid['Vmcf']] = mcf.volume
    i += 1
    y[vid['mmcf']] = mcf.mass
    i += 1
    y[vid['Tmcf']] = mcf.gas.T
    i += 1
    y[vid['Y0mcf']:vid['Y0mcf']+nsp] = mcf.gas.Y
    i += nsp
    # mcb
    y[vid['Vmcb']] = mcb.volume
    i += 1
    y[vid['mmcb']] = mcb.mass
    i += 1
    y[vid['Tmcb']] = mcb.gas.T
    i += 1
    y[vid['Y0mcb']:vid['Y0mcb']+nsp] = mcb.gas.Y
    i += nsp
    # pc
    y[vid['Ppc']] = pc.gas.P
    i += 1
    y[vid['FlamSurfpc']] = 4.*np.pi*(ode.initial_pc_spherical_kernel_radius)**2
    i += 1
    y[vid['mkpc']] = 0.0
    i += 1
    # pcf
    y[vid['Vpcf']] = pcf.volume
    i += 1
    y[vid['mpcf']] = pcf.mass
    i += 1
    y[vid['Tpcf']] = pcf.gas.T
    i += 1
    y[vid['Y0pcf']:vid['Y0pcf']+nsp] = pcf.gas.Y
    i += nsp
    # pcb
    y[vid['Vpcb']] = pcb.volume
    i += 1
    y[vid['mpcb']] = pcb.mass
    i += 1
    y[vid['Tpcb']] = pcb.gas.T
    i += 1
    y[vid['Y0pcb']:vid['Y0pcb']+nsp] = pcb.gas.Y
    i += nsp
    # check
    if i != nvar:
        sys.exit('ERROR check: nvar filled = %d, nvar = %d' % (i, nvar))

    return y
