"""
COR model originates from Ref. [2].

Now that the temperature, speed and composition of the gases exiting
the holes are known, a model is needed to predict whether these gases ignite
the main chamber. For that, the DNS-based COR (for Convected Open Reactor)
model is used. COR consists in following an open reactor, small enough
to be homogeneous, initially full of burnt gases, convected into the chamber:
these hot burnt gases mix at a certain rate with the cold fresh gases of the
main chamber within this open reactor and chemistry inside the reactor can be
tracked using an ODE solver with complex chemistry.

[2] Quentin Malé et al. "Direct numerical simulations and models for hot burnt
gases jet ignition", Combustion and Flame,
https://doi.org/10.1016/j.combustflame.2020.09.017
"""
import numpy as np
import cantera as ct
import scipy.integrate


def dTdxi_func(hk_mol, Wk, wdot, rho, cp, theta0, Yk_in, hk_in, xi):
    """RHS of the temperature ODE, Eq. (29) of Ref. [2]."""
    theta = theta_func(theta0, xi)
    dTdxi = (np.dot(hk_mol, wdot)/rho/cp/theta/xi
             - np.dot(Yk_in, hk_in-hk_mol/Wk)/cp/xi)
    return dTdxi


def dYdxi_func(wdot, Wk, rho, theta0, Yk, Yk_in, xi):
    """RHS of the mass fraction ODEs, Eq. (28) of Ref. [2]."""
    theta = theta_func(theta0, xi)
    dYdxi = - wdot*Wk/rho/theta/xi - (Yk_in - Yk)/xi
    return dYdxi


def theta_func(theta0, xi):
    """Mixing rate function, Eq. (30) of Ref. [2]."""
    theta = theta0*4.*xi*(1. - xi)
    return theta


class ReactorOde:
    """Class of reactor that handles custom ODEs."""
    def __init__(self, gas, P, Yk_in, hk_in, theta0):
        """Initialize the class."""
        # Parameters of the ODE system and auxiliary data
        self.gas = gas
        self.P = P
        self.Yk_in = Yk_in
        self.hk_in = hk_in
        self.theta0 = theta0

    def __call__(self, t, y):
        """The ODE function, y' = f(t,y)."""

        # State vector is [T, Y_1, Y_2, ... Y_K]
        self.gas.set_unnormalized_mass_fractions(y[1:])
        self.gas.TP = y[0], self.P
        xi = t

        dTdxi = dTdxi_func(self.gas.partial_molar_enthalpies,
                           self.gas.molecular_weights,
                           self.gas.net_production_rates, self.gas.density,
                           self.gas.cp, self.theta0, self.Yk_in, self.hk_in,
                           xi)
        dYdxi = dYdxi_func(self.gas.net_production_rates,
                           self.gas.molecular_weights, self.gas.density,
                           self.theta0, self.gas.Y, self.Yk_in, xi)

        return np.hstack((dTdxi, dYdxi))


def COR_model(gas, P, Tu, Yu, Tb, Yb, Uinj, dinj, iduct):
    """COR model to know if ignition is possible or not."""

    # parameters
    K_COR = 0.3
    xi_beg = 0.99
    xi_end = 0.01
    dxi_max = 0.025
    K_dxi = 1.0

    # mc fresh gases properties
    gas.TPY = Tu, P, Yu
    hku = gas.partial_molar_enthalpies/gas.molecular_weights
    hu = gas.h

    # jet burnt gases properties
    gas.TPY = Tb, P, Yb
    hb = gas.h

    # maximum mixing rate
    theta0 = Uinj/dinj*K_COR

    # initial state
    Yi = xi_beg*Yb + (1. - xi_beg)*Yu
    hi = xi_beg*hb + (1. - xi_beg)*hu
    gas.HPY = hi, P, Yi
    Ti = gas.T
    y0 = np.hstack((Ti, Yi))

    # set up objects representing the ODE and the solver
    ode = ReactorOde(gas=gas, P=P, Yk_in=Yu, hk_in=hku, theta0=theta0)
    solver = scipy.integrate.ode(ode)
    solver.set_integrator('vode', method='bdf', with_jacobian=True)
    solver.set_initial_value(y0, xi_beg)

    # first time step evaluation
    dTdxi = dTdxi_func(gas.partial_molar_enthalpies, gas.molecular_weights,
                       gas.net_production_rates, gas.density, gas.cp,
                       theta0, Yu, hku, xi_beg)
    dxi = - min(K_dxi/abs(dTdxi), dxi_max)

    ##########################
    # integrate the equations
    ##########################
    end = False
    while solver.successful() and not end:
        solver.integrate(max(solver.t + dxi, xi_end))

        # update
        gas.TPY = solver.y[0], P, solver.y[1:]

        # time step evaluation
        dTdxi = dTdxi_func(gas.partial_molar_enthalpies, gas.molecular_weights,
                           gas.net_production_rates, gas.density, gas.cp,
                           theta0, Yu, hku, solver.t)
        dxi = - min(K_dxi/abs(dTdxi), dxi_max)

        # termination condition
        if solver.t <= xi_end:
            end = True

    if not solver.successful():
        raise Exception('COR model failed')

    Tend = gas.T
    gas.equilibrate('HP')
    Teq = gas.T

    err = abs(Tend-Teq)/Teq

    if err < 0.10:
        print(' COR MODEL %d -> Uinj = %.0f m/s, Tinj = %.0f K, Teq = %.0f K,\
              Tend = %.0f K => succeeded igntion.' % (iduct, Uinj, Tb, Teq, Tend))
        return True
    else:
        print(' COR MODEL %d -> Uinj = %.0f m/s, Tinj = %.0f K, Teq = %.0f K,\
              Tend = %.0f K => failed igntion.' % (iduct, Uinj, Tb, Teq, Tend))
        return False
