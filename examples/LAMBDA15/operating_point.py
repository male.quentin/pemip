'''Operating point definition.'''

# Engine speed
rpm = 3000.0

# Timing
RFA = -108.
IT = -10.
SOI = -108.
EOI = -70.

# Injection
mflow_inj = 0.000203
Yflow_inj = 'C3H8:1'
Tflow_inj = 298.0

# Initial conditions
P_mc_RFA = 261336.50159814
T_mc_RFA = 371.82856738608

P_pc_RFA = 256229.184861223
T_pc_RFA = 675.048227277247

eqratio_intake = 1.0/1.5

residuals_pc_mass_fraction_RFA = 0.4292044393
residuals_pc_Yk = {}
residuals_pc_Yk['H2O'] = 0.09861337
residuals_pc_Yk['CO2'] = 0.18067806
residuals_pc_Yk['N2'] = 0.72070857

residuals_mc_mass_fraction_RFA = 1.e-4
residuals_mc_Yk = {}
residuals_mc_Yk['H2O'] = 0.09861337
residuals_mc_Yk['CO2'] = 0.18067806
residuals_mc_Yk['N2'] = 0.72070857
