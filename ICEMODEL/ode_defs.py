'''
Contains EngineOde, set of ODEs describing the PEMIP model [1].

[1] Quentin Malé et al. "Jet ignition prediction in a zero- dimensional
pre-chamber engine model", International Journal of Engine Research,
https://doi.org/10.1177/14680874211015002
'''
import numpy as np
from numpy import pi
import scipy
import copy
import cantera as ct

from ICEMODEL.zone_defs import zone
from ICEMODEL.duct_defs import duct as ductobj
from ICEMODEL.useful_functions import get_zpiston
from ICEMODEL.duct_models import get_duct_flow_type, set_zone_duct_flow
from ICEMODEL.COR_model import COR_model
from ICEMODEL.hwall_model import get_hwall


class EngineOde:
    '''
    EngineOde to be called by the solver.
    When called, return y' = f(t, y) = A^(-1) * g(t, y).
    '''
    def init_var_dict(self):
        '''Initialize the dict of variable indices.'''
        self.varIdx = {}
        i = 0
        # mc
        self.varIdx['Vmc'] = i
        i += 1
        self.varIdx['Pmc'] = i
        i += 1
        self.varIdx['FlamSurfmc'] = i
        i += 1
        self.varIdx['mkmc'] = i
        i += 1
        # pcf
        self.varIdx['Vmcf'] = i
        i += 1
        self.varIdx['mmcf'] = i
        i += 1
        self.varIdx['Tmcf'] = i
        i += 1
        for k in range(self.nspecs):
            self.varIdx['Y%dmcf' % k] = i
            i += 1
        # pcb
        self.varIdx['Vmcb'] = i
        i += 1
        self.varIdx['mmcb'] = i
        i += 1
        self.varIdx['Tmcb'] = i
        i += 1
        for k in range(self.nspecs):
            self.varIdx['Y%dmcb' % k] = i
            i += 1
        # pc
        self.varIdx['Ppc'] = i
        i += 1
        self.varIdx['FlamSurfpc'] = i
        i += 1
        self.varIdx['mkpc'] = i
        i += 1
        # pcf
        self.varIdx['Vpcf'] = i
        i += 1
        self.varIdx['mpcf'] = i
        i += 1
        self.varIdx['Tpcf'] = i
        i += 1
        for k in range(self.nspecs):
            self.varIdx['Y%dpcf' % k] = i
            i += 1
        # pcb
        self.varIdx['Vpcb'] = i
        i += 1
        self.varIdx['mpcb'] = i
        i += 1
        self.varIdx['Tpcb'] = i
        i += 1
        for k in range(self.nspecs):
            self.varIdx['Y%dpcb' % k] = i
            i += 1
        self.nvar = i

    def init_ode_dict(self):
        '''Initialize the dict of ode indices.'''
        self.odeIdx = {}
        i = 0
        # mc
        self.odeIdx['mc_volume_variation'] = i
        i += 1
        self.odeIdx['mc_volume_equality'] = i
        i += 1
        self.odeIdx['mc_flame_surface'] = i
        i += 1
        self.odeIdx['mc_turb_kin_energy'] = i
        i += 1
        # mcf
        self.odeIdx['mcf_mass_balance'] = i
        i += 1
        self.odeIdx['mcf_energy_balance'] = i
        i += 1
        self.odeIdx['mcf_ideal_gas_law'] = i
        i += 1
        for k in range(self.nspecs):
            self.odeIdx['mcf_spec_balance_Y%d' % k] = i
            i += 1
        # mcb
        self.odeIdx['mcb_mass_balance'] = i
        i += 1
        self.odeIdx['mcb_energy_balance'] = i
        i += 1
        self.odeIdx['mcb_ideal_gas_law'] = i
        i += 1
        for k in range(self.nspecs):
            self.odeIdx['mcb_spec_balance_Y%d' % k] = i
            i += 1
        # pc
        self.odeIdx['pc_volume_equality'] = i
        i += 1
        self.odeIdx['pc_flame_surface'] = i
        i += 1
        self.odeIdx['pc_turb_kin_energy'] = i
        i += 1
        # pcf
        self.odeIdx['pcf_mass_balance'] = i
        i += 1
        self.odeIdx['pcf_energy_balance'] = i
        i += 1
        self.odeIdx['pcf_ideal_gas_law'] = i
        i += 1
        for k in range(self.nspecs):
            self.odeIdx['pcf_spec_balance_Y%d' % k] = i
            i += 1
        # pcb
        self.odeIdx['pcb_mass_balance'] = i
        i += 1
        self.odeIdx['pcb_energy_balance'] = i
        i += 1
        self.odeIdx['pcb_ideal_gas_law'] = i
        i += 1
        for k in range(self.nspecs):
            self.odeIdx['pcb_spec_balance_Y%d' % k] = i
            i += 1
        # check
        if i != self.nvar:
            print('ERROR check: node = %d, nvar = %d' % (i, self.nvar))
            exit()

    def init_zones(self, cti, nspec, nd):
        '''Initialize the zones.'''
        # mc and pc zones
        self.mc = zone(cti, 'mc', self.sl_table)
        self.mcf = zone(cti, 'mcf', self.sl_table)
        self.mcb = zone(cti, 'mcb', self.sl_table)
        self.pc = zone(cti, 'pc', self.sl_table)
        self.pcf = zone(cti, 'pcf', self.sl_table)
        self.pcb = zone(cti, 'pcb', self.sl_table)
        self.zoneList = [self.mc, self.mcf, self.mcb,
                         self.pc, self.pcf, self.pcb]

        # duct
        self.duct = ductobj(nspec, nd)

    def reset_zone_flows(self):
        '''Reset the zone flows to empty list.'''
        for zone in self.zoneList:
            zone.reset_flow()

    def calc_duct_flow(self):
        '''Compute the flow properties through the ducts.'''
        # -> get flow mixture type (burnt/unburnt)
        flowType = get_duct_flow_type(self)

        # -> set the flow variables of the zones
        if self.mc.gas.P > self.pc.gas.P:  # from mc -> pc
            if flowType == 'fresh':
                (self.mcf, self.pcf, self.mc, self.pc,
                 self.duct) = set_zone_duct_flow(zin=self.mcf, zout=self.pcf,
                                                 gbl_zin=self.mc,
                                                 gbl_zout=self.pc,
                                                 duct=self.duct, gas=self.gas,
                                                 Tw=self.engineDefs.Twall,
                                                 duct_model='analytic',
                                                 force_equil=False)
            else:
                (self.mcb, self.pcb, self.mc, self.pc,
                 self.duct) = set_zone_duct_flow(zin=self.mcb,
                                                 zout=self.pcb,
                                                 gbl_zin=self.mc,
                                                 gbl_zout=self.pc,
                                                 duct=self.duct,
                                                 gas=self.gas,
                                                 Tw=self.engineDefs.Twall,
                                                 duct_model='analytic',
                                                 force_equil=False)

        elif self.pc.gas.P > self.mc.gas.P:  # from pc -> mc
            if flowType == 'fresh':
                (self.pcf, self.mcf, self.pc, self.mc,
                 self.duct) = set_zone_duct_flow(zin=self.pcf, zout=self.mcf,
                                                 gbl_zin=self.pc,
                                                 gbl_zout=self.mc,
                                                 duct=self.duct, gas=self.gas,
                                                 Tw=self.engineDefs.Twall,
                                                 duct_model='analytic',
                                                 force_equil=False)
            else:
                duct_model = 'auto'
                force_equil = False
                (self.pcb, self.mcb, self.pc, self.mc,
                 self.duct) = set_zone_duct_flow(zin=self.pcb, zout=self.mcb,
                                                 gbl_zin=self.pc,
                                                 gbl_zout=self.mc,
                                                 duct=self.duct, gas=self.gas,
                                                 Tw=self.engineDefs.Twall,
                                                 duct_model=duct_model,
                                                 force_equil=force_equil)

    def calc_inj_flow(self):
        '''
        Compute the flow properties through the injector nozzle.
        '''
        if self.CAD >= self.opDefs.SOI and self.CAD < self.opDefs.EOI:
            # -> injection inside the pre chamber
            self.gas.TPY = (self.opDefs.Tflow_inj, self.pc.gas.P,
                            self.opDefs.Yflow_inj)
            self.pc.mflow_in.append(self.opDefs.mflow_inj)
            self.pc.hflow_in.append(copy.deepcopy(self.gas.enthalpy_mass))
            self.pc.Yflow_in.append(copy.deepcopy(self.gas.Y))
            self.pcf.mflow_in.append(self.opDefs.mflow_inj)
            self.pcf.hflow_in.append(copy.deepcopy(self.gas.enthalpy_mass))
            self.pcf.Yflow_in.append(copy.deepcopy(self.gas.Y))

    def calc_heat_losses(self):
        '''
        Compute heat losses, Eq. (15) of Ref. [1].
        '''
        # -> main chamber
        # mc wall surface
        self.mc.Swall = (
            self.engineDefs.S_piston + self.engineDefs.S_head
            + get_zpiston(self.theta, self.engineDefs.stroke,
                          self.engineDefs.conrod, self.engineDefs.bore)
            *pi*self.engineDefs.bore)
        for z in [self.mcf, self.mcb]:
            # zone wall surface
            z.Swall = self.mc.Swall*z.volume/self.mc.volume
            # zone h coef
            z.hwall = get_hwall(L=self.engineDefs.bore,
                                N=self.opDefs.rpm,
                                stroke=self.engineDefs.stroke,
                                nu=z.gas.viscosity/z.gas.density,
                                lbda=z.gas.thermal_conductivity,
                                a=self.mc_uw)
            # zone heat losses
            z.Qwall = z.hwall*z.Swall*(self.engineDefs.T_wall - z.gas.T)

        # -> pre chamber
        for z in [self.pcf, self.pcb]:
            # zone wall surface
            z.Swall = self.pc.Swall*z.volume/self.pc.volume
            # zone h coef
            z.hwall = get_hwall(L=self.engineDefs.d_pc,
                                N=self.opDefs.rpm,
                                stroke=self.engineDefs.stroke,
                                nu=z.gas.viscosity/z.gas.density,
                                lbda=z.gas.thermal_conductivity,
                                a=self.pc_uw)
            # zone heat losses
            z.Qwall = z.hwall*z.Swall*(self.engineDefs.T_wall - z.gas.T)

    def get_mc_flame_surface_source_term(self):
        '''
        Compute the source term of the mc flame surface ode, Eq. (42) of
        Ref. [1].
        '''
        if self.mc.comb_started and not self.mc.comb_ended:
            # combustion is occuring
            # -> equation for flame surface
            bf = self.mcb.mass/self.mc.mass
            # dS/dt = a*e/k*S - b*SL*S**2/(1-c)
            wdot = (self.mc_alpha*self.mc.eps/self.mc.k*self.mc.FlamSurf
                    - self.mc_beta*self.mcf.SL*self.mc.FlamSurf**2
                    /max(np.finfo(float).tiny, (1. - bf))/self.mc.volume)
            if (self.mc.burnt_gases_injection_started
                and (not self.mc.burnt_gases_injection_ended)
                and self.pc.gas.P > self.mc.gas.P):
                # Adding of a source term to account for flame surface creation
                # due to burnt gases injection
                for i in range(len(self.duct.d)):
                    wdot += (self.duct.N[i]*pi*self.duct.d[i]
                             *max(self.duct.Uout[i], 0.0)*self.KwAmcInj)
        else:
            # there is no combustion
            wdot = 0.

        return wdot

    def calc_mc_combustion(self):
        '''
        Compute the main chamber combustion related variables.
        '''
        # bring the fresh mixture to equilibrium
        self.gas.TPY = self.mcf.gas.T, self.mcf.gas.P, self.mcf.gas.Y
        self.gas.equilibrate('HP')
        Yflow = self.gas.Y
        hflow = self.gas.enthalpy_mass

        # mass flow through the flame
        mflow = self.mcf.gas.density*self.mcf.SL*self.mc.FlamSurf

        # add flow from mcf to mcb
        self.mcf.mflow_out.append(copy.deepcopy(mflow))
        self.mcb.mflow_in.append(copy.deepcopy(mflow))
        self.mcb.hflow_in.append(copy.deepcopy(hflow))
        self.mcb.Yflow_in.append(copy.deepcopy(Yflow))

    def get_mc_mk_RHS(self):
        '''
        Compute the RHS terms of the ODE for the mc turbulent kinetic energy
        dmkdt = RHS, Eq. (37) of Ref. [1].
        '''
        RHS = 0.

        # jet production term
        if self.pc.gas.P > self.mc.gas.P:
            for i in range(len(self.duct.d)):
                RHS += 0.5*self.duct.mflow[i]*self.duct.Uout[i]**2

        # destruction term
        RHS -= self.mc.mass*self.mc.eps

        return RHS

    def get_pc_flame_surface_source_term(self):
        '''
        Compute the source term of the pc flame surface ode, Eq. (41) of
        Ref. [1].
        '''
        if self.pc.comb_started and not self.pc.comb_ended:
            # -> equation for flame surface
            bf = self.pcb.mass/self.pc.mass
            # dS/dt = a*e/k*S - b*SL*S**2/(1-c)
            wdot = (self.pc_alpha*self.pc.eps/self.pc.k*self.pc.FlamSurf
                    - self.pc_beta*self.pcf.SL*self.pc.FlamSurf**2
                    /max(np.finfo(float).tiny, (1. - bf))/self.pc.volume)
        else:
            # there is no combustion
            wdot = 0.

        return wdot

    def calc_pc_combustion(self):
        '''
        Compute the pre chamber combustion related variables.
        '''
        # bring the fresh mixture to equilibrium
        self.gas.TPY = self.pcf.gas.T, self.pcf.gas.P, self.pcf.gas.Y
        self.gas.equilibrate('HP')
        Yflow = self.gas.Y
        hflow = self.gas.enthalpy_mass

        # mass flow through the flame
        mflow = self.pcf.gas.density*self.pcf.SL*self.pc.FlamSurf

        # add flow from pcf to pcb
        self.pcf.mflow_out.append(copy.deepcopy(mflow))
        self.pcb.mflow_in.append(copy.deepcopy(mflow))
        self.pcb.hflow_in.append(copy.deepcopy(hflow))
        self.pcb.Yflow_in.append(copy.deepcopy(Yflow))

    def get_pc_mk_RHS(self):
        '''
        Compute the RHS terms of the ODE for the pc turbulent kinetic energy
        dmkdt = RHS, Eq. (37) of Ref. [1].
        '''
        RHS = 0.

        # jet production term
        if self.mc.gas.P > self.pc.gas.P:
            for i in range(len(self.duct.d)):
                RHS += 0.5*self.duct.mflow[i]*self.duct.Uout[i]**2

        # destruction term
        RHS -= self.pc.mass*self.pc.eps

        return RHS

    def check_ignition_criterion(self, gas, mcf, Tjet, Yjet, Ujet, djet,
                                 iduct):
        '''
        Call an external model to check if ignition is possible or not.
        Here COR model [2].
        [2] Quentin Malé et al. "Direct numerical simulations and models for
        hot burnt gases jet ignition", Combustion and Flame,
        https://doi.org/10.1016/j.combustflame.2020.09.017
        '''
        flag = COR_model(gas=gas,
                         P=mcf.gas.P,
                         Tu=mcf.gas.T,
                         Yu=mcf.gas.Y,
                         Tb=Tjet,
                         Yb=Yjet,
                         Uinj=Ujet,
                         dinj=djet,
                         iduct=iduct)

        return flag

    def update_zone_variables(self, t, y):
        '''
        Compute all the zone variables from the y vector of variables.
        '''
        # easier var names
        vid = self.varIdx
        nsp = self.nspecs

        # -> time to CAD (deg) and theta (rad)
        self.CAD = self.opDefs.RFA + self.degps*t
        self.theta = self.CAD*pi/180.

        # -> vector of variables
        i = 0
        # mc
        self.mc.volume = y[vid['Vmc']]
        i += 1
        self.mc.FlamSurf = y[vid['FlamSurfmc']]
        i += 1
        self.mc.mk = y[vid['mkmc']]
        i += 1
        # mcf
        self.mcf.volume = y[vid['Vmcf']]
        i += 1
        self.mcf.mass = y[vid['mmcf']]
        i += 1
        self.mcf.gas.TPY = (y[vid['Tmcf']], y[vid['Pmc']],
                            y[vid['Y0mcf']:vid['Y0mcf']+nsp])
        i += 2 + nsp
        # mcb
        self.mcb.volume = y[vid['Vmcb']]
        i += 1
        self.mcb.mass = y[vid['mmcb']]
        i += 1
        self.mcb.gas.TPY = (y[vid['Tmcb']], y[vid['Pmc']],
                            y[vid['Y0mcb']:vid['Y0mcb']+nsp])
        i += 2 + nsp - 1
        # pc
        self.pc.FlamSurf = y[vid['FlamSurfpc']]
        i += 1
        self.pc.mk = y[vid['mkpc']]
        i += 1
        # pcf
        self.pcf.volume = y[vid['Vpcf']]
        i += 1
        self.pcf.mass = y[vid['mpcf']]
        i += 1
        self.pcf.gas.TPY = (y[vid['Tpcf']], y[vid['Ppc']],
                            y[vid['Y0pcf']:vid['Y0pcf']+nsp])
        i += 2 + nsp
        # pcb
        self.pcb.volume = y[vid['Vpcb']]
        i += 1
        self.pcb.mass = y[vid['mpcb']]
        i += 1
        self.pcb.gas.TPY = (y[vid['Tpcb']], y[vid['Ppc']],
                            y[vid['Y0pcb']:vid['Y0pcb']+nsp])
        i += 2 + nsp - 1
        # check
        if i != self.nvar:
            print('ERROR check: used var = %d, nvar = %d' % (i, self.nvar))
            exit()

        # -> compute mc properties from ideal gas law
        self.mc.mass = self.mcf.mass + self.mcb.mass
        self.mc.gas.Y = (self.mcf.mass*self.mcf.gas.Y
                         + self.mcb.mass*self.mcb.gas.Y)/self.mc.mass
        self.mc.gas.TPY = (
            (y[vid['Pmc']]*self.mc.volume*self.mc.gas.mean_molecular_weight
             /self.mc.mass/ct.gas_constant),
            y[vid['Pmc']],
            (self.mcf.mass*self.mcf.gas.Y + self.mcb.mass*self.mcb.gas.Y)
            /self.mc.mass)

        # -> compute pc properties from ideal gas law
        self.pc.mass = self.pcf.mass + self.pcb.mass
        self.pc.gas.Y = (self.pcf.mass*self.pcf.gas.Y
                         + self.pcb.mass*self.pcb.gas.Y)/self.pc.mass
        self.pc.gas.TPY = (
            (y[vid['Ppc']]*self.pc.volume*self.pc.gas.mean_molecular_weight
             /self.pc.mass/ct.gas_constant),
            y[vid['Ppc']],
            (self.pcf.mass*self.pcf.gas.Y + self.pcb.mass*self.pcb.gas.Y)
            /self.pc.mass)

        # -> turbulence properties
        self.pc.k = self.pc.mk/self.pc.mass
        self.pc.eps = (2.*self.pc.k/3.)**(1.5)/self.pc.lt
        self.mc.k = self.mc.mk/self.mc.mass
        self.mc.eps = (2.*self.mc.k/3.)**(1.5)/self.mc.lt

        # -> compute phi
        for zone in self.zoneList:
            zone.compute_phi()

        # -> reset flow vars
        self.reset_zone_flows()

        # -> compute what it is going through the injector
        self.calc_inj_flow()

        # -> burnt gases injection flag
        if (self.pcb.mass/self.pc.mass >= self.pc_bf_thr
            and self.pc.gas.P > self.mc.gas.P
            and not self.mc.burnt_gases_injection_started):
            self.mc.burnt_gases_injection_started = True
            self.mc_burnt_gases_injection_start = self.CAD
            print(' INFO: burnt gases injection started!')

        if (self.mc.burnt_gases_injection_started
            and self.pc.gas.P < self.mc.gas.P
            and not self.mc.burnt_gases_injection_ended):
            self.mc.burnt_gases_injection_ended = True
            self.mc_burnt_gases_injection_end = self.CAD
            print(' INFO: burnt gases injection ended!')

        # -> compute what it is going through the ducts
        self.calc_duct_flow()

        # -> heat losses
        self.calc_heat_losses()

        # -> pre chamber combustion
        if (self.CAD >= (self.opDefs.IT + self.dI)
           and not self.pc.comb_started):
            self.pc.comb_started = True
            self.pc_comb_start = self.CAD
            print(' INFO: pre chamber combustion started')

        if (self.pcb.mass/self.pc.mass >= 0.99
           and self.pc.comb_started
           and not self.pc.comb_ended):
            self.pc.comb_ended = True
            self.pc_comb_end = self.CAD
            print(' INFO: pre chamber combustion ended!')

        if self.pc.comb_started and not self.pc.comb_ended:
            self.pcf.compute_laminar_flame_speed()
            self.calc_pc_combustion()
        else:
            self.pcf.SL = 0.

        if (self.mc.burnt_gases_injection_started
            and not np.sum(self.duct.TJI) == len(self.duct.d)
            and not self.mc.burnt_gases_injection_ended
            and self.CAD >= self.mcign_eval_CAD):
            self.mcign_eval_CAD = self.CAD + self.mcign_eval_dCAD
            dP = np.abs(self.pc.gas.P-self.mc.gas.P)
            for i in range(len(self.duct.d)):
                if self.duct.TJI[i] == 0 and dP >= self.dP_TJI_thr:
                    # check TJI
                    if self.check_ignition_criterion(gas=self.gas,
                                                     mcf=self.mcf,
                                                     Tjet=self.duct.Tout[i],
                                                     Yjet=self.duct.Yout[i, :],
                                                     Ujet=self.duct.Uout[i],
                                                     djet=self.duct.d[i],
                                                     iduct=i):
                        self.duct.TJI[i] = 1

        # -> main chamber combustion
        if (self.mc.burnt_gases_injection_started
            and not self.mc.comb_started
            and not self.mc.burnt_gases_injection_ended):
            if np.sum(self.duct.TJI) > 0:
                self.mc.comb_started = True
                self.mc_comb_start = self.CAD
                print(' INFO: main chamber combustion started')

        if (self.mcb.mass/self.mc.mass >= 0.99
            and self.mc.comb_started
            and not self.mc.comb_ended):
            self.mc.comb_ended = True
            self.mc_comb_end = self.CAD
            print(' INFO: main chamber combustion ended!')

        if self.mc.comb_started and not self.mc.comb_ended:
            self.mcf.compute_laminar_flame_speed()
            self.calc_mc_combustion()
        else:
            self.mcf.SL = 0.

    def __init__(self, cti, engineDefs, opDefs, sl_table):
        '''
        Init function.
        '''

        # dummy gas object
        self.gas = ct.Solution(cti)
        # engine definitions
        self.engineDefs = engineDefs
        # operating point definitions
        self.opDefs = opDefs
        # flame speed table
        self.sl_table = sl_table
        # usefull quantities (SI)
        self.nspecs = self.gas.n_species
        self.Wk = self.gas.molecular_weights*1e-3
        self.R = ct.gas_constant*1e-3
        self.degps = self.opDefs.rpm*360./60.
        # initialize the dict of variables
        self.init_var_dict()
        # initialize the dict of odes
        self.init_ode_dict()
        # initialize the zones
        self.init_zones(cti, self.gas.n_species, len(self.engineDefs.d_ducts))
        # set the constant pc volume
        self.pc.volume = self.engineDefs.V_pc
        # set the constant pc surface
        self.pc.Swall = self.engineDefs.S_pc
        # duct geometry
        self.duct.d = self.engineDefs.d_ducts
        self.duct.l = self.engineDefs.l_ducts
        self.duct.N = self.engineDefs.n_ducts
        self.duct.S = np.zeros_like(self.duct.d)
        self.duct.Stot = 0.
        for i in range(len(self.duct.d)):
            self.duct.S[i] = pi*self.duct.d[i]**2/4.
            self.duct.Stot += self.duct.S[i]*self.duct.N[i]
        self.duct.dm = np.sqrt(4.*self.duct.Stot/np.sum(self.duct.N)/pi)
        self.duct.TJI = np.zeros_like(self.duct.d)

        # integral length scale
        self.mc.lt = self.duct.dm
        self.pc.lt = self.duct.dm

        # ignition evaluation
        self.mcign_eval_CAD = -1e120
        self.mcign_eval_dCAD = 0.1

        # -> parameters
        self.initial_pc_spherical_kernel_radius = 1.5e-3
        # spark ignition delay
        self.dI = 1.25

        # pc burnt fraction from which burnt gases are exhausted
        self.pc_bf_thr = 0.75
        # flame surface ode parameters
        self.pc_alpha = 1.2
        self.pc_beta = 3.6
        self.mc_alpha = 0.45
        self.mc_beta = 2.0
        self.KwAmcInj = 10.0
        # heat losses parameters
        self.pc_uw = 0.200
        self.mc_uw = 0.445
        # dP thr below TJI is considered ended
        self.dP_TJI_thr = 1.e5

        # flags
        self.pc.comb_started = False
        self.mc.comb_started = False
        self.pc.comb_ended = False
        self.mc.comb_ended = False
        self.mc.burnt_gases_injection_started = False
        self.mc.burnt_gases_injection_ended = False

        # others
        self.pc_comb_start = None
        self.pc_comb_end = None
        self.mc_comb_start = None
        self.mc_comb_end = None
        self.mc_burnt_gases_injection_start = None
        self.mc_burnt_gases_injection_end = None

    def __call__(self, t, y):
        '''
        Call function. When called, return
        y' = f(t, y) = A^(-1)*g(t, y).
        '''

        # easier var names
        vid = self.varIdx
        oid = self.odeIdx
        nsp = self.nspecs

        # -> update zone variables from y vector and time
        self.update_zone_variables(t, y)

        # <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
        #                       COMPUTATIONS
        #    A * y' = g(t,y)
        #    =>  y' = f(t,y) = A^-1 * g(t,y)
        # <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

        # -> array initialization
        g = np.zeros(self.nvar, 'd')
        A = np.zeros((self.nvar, self.nvar), 'd')

        ######################################
        #      COMMON MAIN CHAMBER ZONE
        ######################################

        # -> geometrical law from piston mvt, Eq. (14) of Ref. [1]
        i = oid['mc_volume_variation']
        # RHS
        RHS = 0.25*self.engineDefs.stroke*np.cos(self.theta)
        RHS /= (self.engineDefs.conrod**2
                - (0.5*self.engineDefs.stroke*np.sin(self.theta))**2)**(0.5)
        RHS += 0.5
        RHS *= self.engineDefs.stroke*np.sin(self.theta)
        RHS *= pi/180.*self.degps*pi*self.engineDefs.bore**2/4.
        g[i] = copy.deepcopy(RHS)
        # Matrix row
        A[i, vid['Vmc']] = 1.

        # -> main chamber volumes equation, Eq. (13) of Ref. [1]
        i = oid['mc_volume_equality']
        # RHS
        g[i] = 0.
        # Matrix row
        A[i, vid['Vmcf']] = 1.
        A[i, vid['Vmcb']] = 1.
        A[i, vid['Vmc']] = -1.

        # -> main chamber flame surface equation, Eq. (42) of Ref. [1]
        i = oid['mc_flame_surface']
        # RHS
        g[i] = self.get_mc_flame_surface_source_term()
        # Matrix row
        A[i, vid['FlamSurfmc']] = 1.

        # -> main chamber turbulent kinetic energy equation, Eq. (37) of
        # Ref. [1]
        i = oid['mc_turb_kin_energy']
        # RHS
        g[i] = self.get_mc_mk_RHS()
        # Matrix row
        A[i, vid['mkmc']] = 1.

        ######################################
        #       FRESH MAIN CHAMBER ZONE
        ######################################

        # -> mcf mass balance equation, Eq. (7) of Ref. [1]
        i = oid['mcf_mass_balance']
        # RHS
        zone = self.mcf
        RHS = sum(zone.mflow_in) - sum(zone.mflow_out)
        g[i] = copy.deepcopy(RHS)
        # Matrix row
        A[i, vid['mmcf']] = 1.

        # -> mcf energy balance equation (1P), Eq. (11) of Ref. [1]
        i = oid['mcf_energy_balance']
        # RHS
        zone = self.mcf
        # preliminary calculations
        uk = np.divide(zone.gas.partial_molar_int_energies,
                       zone.gas.molecular_weights)
        P = zone.gas.P
        V = zone.volume
        m = zone.mass
        # RHS from 1st principe
        RHS = 0.
        RHS += + zone.Qwall
        for f in range(len(zone.mflow_in)):
            RHS += + zone.mflow_in[f]*(zone.hflow_in[f]
                                       - np.dot(uk[:], zone.Yflow_in[f][:]))
        RHS += - P*V/m*sum(zone.mflow_out)
        g[i] = copy.deepcopy(RHS)
        # Matrix row
        A[i, vid['Vmcf']] = self.mcf.gas.P
        A[i, vid['Tmcf']] = self.mcf.mass*self.mcf.gas.cv_mass

        # -> mcf ideal gas law, Eq. (5) of Ref. [1]
        i = oid['mcf_ideal_gas_law']
        # RHS
        g[i] = 0.
        # Matrix row
        A[i, vid['Vmcf']] = +self.mcf.gas.P
        A[i, vid['mmcf']] = (
            -self.R/self.mcf.gas.mean_molecular_weight*1e3*self.mcf.gas.T)
        A[i, vid['Tmcf']] = (
            -self.R/self.mcf.gas.mean_molecular_weight*1e3*self.mcf.mass)
        A[i, vid['Pmc']] = +self.mcf.volume
        A[i, vid['Y0mcf']:vid['Y0mcf']+nsp] = (
            -self.R*self.mcf.gas.T*self.mcf.mass/self.Wk[:])

        # -> mcf species mass conservation, Eq. (6) of Ref. [1]
        i = oid['mcf_spec_balance_Y0']
        # RHS
        zone = self.mcf
        for k in range(nsp):
            RHS = 0.
            for f in range(len(zone.mflow_in)):
                RHS += zone.mflow_in[f]*(zone.Yflow_in[f][k] - zone.gas.Y[k])
            g[i+k] = copy.deepcopy(RHS)
        # Matrix rows
        for k in range(nsp):
            A[i+k, i+k] = self.mcf.mass

        ######################################
        #      BURNT MAIN CHAMBER ZONE
        ######################################

        # -> mcb mass balance equation, Eq. (7) of Ref. [1]
        i = oid['mcb_mass_balance']
        # RHS
        zone = self.mcb
        RHS = sum(zone.mflow_in) - sum(zone.mflow_out)
        g[i] = copy.deepcopy(RHS)
        # Matrix row
        A[i, vid['mmcb']] = 1.

        # -> mcb energy balance equation (1P), Eq. (11) of Ref. [1]
        i = oid['mcb_energy_balance']
        # RHS
        zone = self.mcb
        # preliminary calculations
        uk = np.divide(zone.gas.partial_molar_int_energies,
                       zone.gas.molecular_weights)
        P = zone.gas.P
        V = zone.volume
        m = zone.mass
        # RHS from 1st principe
        RHS = 0.
        RHS += + zone.Qwall
        for f in range(len(zone.mflow_in)):
            RHS += + zone.mflow_in[f]*(zone.hflow_in[f]
                                       - np.dot(uk[:], zone.Yflow_in[f][:]))
        RHS += - P*V/m*sum(zone.mflow_out)
        g[i] = copy.deepcopy(RHS)
        # Matrix row
        A[i, vid['Vmcb']] = self.mcb.gas.P
        A[i, vid['Tmcb']] = self.mcb.mass*self.mcb.gas.cv_mass

        # -> mcb ideal gas law, Eq. (5) of Ref. [1]
        i = oid['mcb_ideal_gas_law']
        # RHS
        g[i] = 0.
        # Matrix row
        A[i, vid['Vmcb']] = +self.mcb.gas.P
        A[i, vid['mmcb']] = (
            -self.R/self.mcb.gas.mean_molecular_weight*1e3*self.mcb.gas.T)
        A[i, vid['Tmcb']] = (
            -self.R/self.mcb.gas.mean_molecular_weight*1e3*self.mcb.mass)
        A[i, vid['Pmc']] = +self.mcb.volume
        A[i, vid['Y0mcb']:vid['Y0mcb']+nsp] = (
            -self.R*self.mcb.gas.T*self.mcb.mass/self.Wk[:])

        # -> mcb species mass conservation, Eq. (6) of Ref. [1]
        i = oid['mcb_spec_balance_Y0']
        # RHS
        zone = self.mcb
        for k in range(nsp):
            RHS = 0.
            for f in range(len(zone.mflow_in)):
                RHS += zone.mflow_in[f]*(zone.Yflow_in[f][k] - zone.gas.Y[k])
            g[i+k] = copy.deepcopy(RHS)
        # Matrix rows
        for k in range(nsp):
            A[i+k, i+k] = self.mcb.mass

        ######################################
        #       COMMON PRE CHAMBER ZONE
        ######################################

        # -> pre chamber volumes equation, Eq. (12) of Ref. [1]
        i = oid['pc_volume_equality']
        # RHS
        g[i] = 0.
        # Matrix row
        A[i, vid['Vpcf']] = 1.
        A[i, vid['Vpcb']] = 1.

        # -> pre chamber flame surface equation, Eq. (41) of Ref. [1]
        i = oid['pc_flame_surface']
        # RHS
        g[i] = self.get_pc_flame_surface_source_term()
        # Matrix row
        A[i, vid['FlamSurfpc']] = 1.

        # -> pre chamber turbulent kinetic energy equation, Eq. (37) of
        # Ref. [1]
        i = oid['pc_turb_kin_energy']
        # RHS
        g[i] = self.get_pc_mk_RHS()
        # Matrix row
        A[i, vid['mkpc']] = 1.

        ######################################
        #       FRESH PRE CHAMBER ZONE
        ######################################

        # -> pcf mass balance equation, Eq. (7) of Ref. [1]
        i = oid['pcf_mass_balance']
        # RHS
        zone = self.pcf
        RHS = sum(zone.mflow_in) - sum(zone.mflow_out)
        g[i] = copy.deepcopy(RHS)
        # Matrix row
        A[i, vid['mpcf']] = 1.

        # -> pcf energy balance equation (1P), Eq. (11) of Ref. [1]
        i = oid['pcf_energy_balance']
        # RHS
        zone = self.pcf
        # preliminary calculations
        uk = np.divide(zone.gas.partial_molar_int_energies,
                       zone.gas.molecular_weights)
        P = zone.gas.P
        V = zone.volume
        m = zone.mass
        # RHS from 1st principe
        RHS = 0.
        RHS += + zone.Qwall
        for f in range(len(zone.mflow_in)):
            RHS += + zone.mflow_in[f]*(zone.hflow_in[f]
                                       - np.dot(uk[:], zone.Yflow_in[f][:]))
        RHS += - P*V/m*sum(zone.mflow_out)
        g[i] = copy.deepcopy(RHS)
        # Matrix row
        A[i, vid['Vpcf']] = self.pcf.gas.P
        A[i, vid['Tpcf']] = self.pcf.mass*self.pcf.gas.cv_mass

        # -> pcf ideal gas law, Eq. (5) of Ref. [1]
        i = oid['pcf_ideal_gas_law']
        # RHS
        g[i] = 0.
        # Matrix row
        A[i, vid['Vpcf']] = +self.pcf.gas.P
        A[i, vid['mpcf']] = (
            -self.R/self.pcf.gas.mean_molecular_weight*1e3*self.pcf.gas.T)
        A[i, vid['Tpcf']] = (
            -self.R/self.pcf.gas.mean_molecular_weight*1e3*self.pcf.mass)
        A[i, vid['Ppc']] = +self.pcf.volume
        A[i, vid['Y0pcf']:vid['Y0pcf']+nsp] = (
            -self.R*self.pcf.gas.T*self.pcf.mass/self.Wk[:])

        # -> pcf species mass conservation, Eq. (6) of Ref. [1]
        i = oid['pcf_spec_balance_Y0']
        # RHS
        zone = self.pcf
        for k in range(nsp):
            RHS = 0.
            for f in range(len(zone.mflow_in)):
                RHS += zone.mflow_in[f]*(zone.Yflow_in[f][k] - zone.gas.Y[k])
            g[i+k] = copy.deepcopy(RHS)
        # Matrix rows
        for k in range(nsp):
            A[i+k, i+k] = self.pcf.mass

        ######################################
        #       BURNT PRE CHAMBER ZONE
        ######################################

        # -> pcb mass balance equation, Eq. (7) of Ref. [1]
        i = oid['pcb_mass_balance']
        # RHS
        zone = self.pcb
        RHS = sum(zone.mflow_in) - sum(zone.mflow_out)
        g[i] = copy.deepcopy(RHS)
        # Matrix row
        A[i, vid['mpcb']] = 1.

        # pcb energy balance equation (1P), Eq. (11) of Ref. [1]
        i = oid['pcb_energy_balance']
        # RHS
        zone = self.pcb
        # preliminary calculations
        uk = np.divide(zone.gas.partial_molar_int_energies,
                       zone.gas.molecular_weights)
        P = zone.gas.P
        V = zone.volume
        m = zone.mass
        # RHS from 1st principe
        RHS = 0.
        RHS += + zone.Qwall
        for f in range(len(zone.mflow_in)):
            RHS += zone.mflow_in[f]*(zone.hflow_in[f]
                                     - np.dot(uk[:], zone.Yflow_in[f][:]))
        RHS += - P*V/m*sum(zone.mflow_out)
        g[i] = copy.deepcopy(RHS)
        # Matrix row
        A[i, vid['Vpcb']] = self.pcb.gas.P
        A[i, vid['Tpcb']] = self.pcb.mass*self.pcb.gas.cv_mass

        # pcb ideal gas law, Eq. (5) of Ref. [1]
        i = oid['pcb_ideal_gas_law']
        # RHS
        g[i] = 0.
        # Matrix row
        A[i, vid['Vpcb']] = +self.pcb.gas.P
        A[i, vid['mpcb']] = (
            -self.R/self.pcb.gas.mean_molecular_weight*1e3*self.pcb.gas.T)
        A[i, vid['Tpcb']] = (
            -self.R/self.pcb.gas.mean_molecular_weight*1e3*self.pcb.mass)
        A[i, vid['Ppc']] = +self.pcb.volume
        A[i, vid['Y0pcb']:vid['Y0pcb']+nsp] = (
            -self.R*self.pcb.gas.T*self.pcb.mass/self.Wk[:])

        # pcb species mass conservation, Eq. (6) of Ref. [1]
        i = oid['pcb_spec_balance_Y0']
        # RHS
        zone = self.pcb
        for k in range(nsp):
            RHS = 0.
            for f in range(len(zone.mflow_in)):
                RHS += zone.mflow_in[f]*(zone.Yflow_in[f][k] - zone.gas.Y[k])
            g[i+k] = copy.deepcopy(RHS)
        # Matrix rows
        for k in range(nsp):
            A[i+k, i+k] = self.pcb.mass

        # <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
        #                   y' = f(t,y) = A^-1 * g(t,y)
        # <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

        # Matrix inversion
        Ainv = scipy.linalg.inv(A)
        # f(t, y) = A^-1 * g(t,y), Eq. (45) of Ref. [1]
        yprim = np.matmul(Ainv, g)

        return yprim
