'''Zone class definition.'''
import numpy as np
import cantera as ct
import h5py
from scipy.interpolate import interpn


class zone:

    def __init__(self, cti, name, sl_table):
        """Initialize variables."""

        # zone creation
        self.gas = ct.Solution(cti)
        self.name = name

        # init variables
        self.mass = 0.
        self.volume = 0.
        self.mflow_in = []
        self.hflow_in = []
        self.Yflow_in = []
        self.mflow_out = []
        self.Qwall = 0.
        self.Swall = 0.
        self.hwall = 0.
        self.phi = 0.
        self.FlamSurf = 0.
        self.mk = 0.
        self.k = 0.
        self.eps = 0.
        self.lt = 0.
        self.SL = 0.

        # import the flame speed table
        with h5py.File(sl_table, 'r') as h5f:
            self.sl_phiList = h5f['equivalence_ratio'][()]
            self.sl_tempeList = h5f['temperature'][()]
            self.sl_pressList = h5f['pressure'][()]
            self.sl_table = h5f['laminar_flame_speed'][()]
            if 'Yfadd' in h5f.keys():
                self.sl_YfaddList = h5f['Yfadd'][()]
                self.h2table = True
            else:
                self.h2table = False
        return

    def reset_flow(self):
        '''Reset the flow arrays.'''
        self.mflow_in = []
        self.hflow_in = []
        self.Yflow_in = []
        self.mflow_out = []
        return

    def print_me(self):
        '''Print informations.'''
        print(" * zone name: " + self.name)
        print("   mass      = %.3e kg" % self.mass)
        print("   vol       = %.3e m3" % self.volume)
        print("   tempe     = %.2f K" % self.gas.T)
        print("   press     = %.3e Pa" % self.gas.P)
        print("   Qwall     = %.3e J/s" % self.Qwall)
        print("   Swall     = %.3e m2" % self.Swall)
        if self.FlameSurf != 0.:
            print("   FlameSurfDens = %.3e m2/m3" % self.FlameSurfDens)
        for fl in range(len(self.mflow_out)):
            print("   mflow_out %d = %.3e kg/s" % (fl+1, self.mflow_out[fl]))
        for fl in range(len(self.mflow_in)):
            print("   mflow_in %d = %.3e kg/s" % (fl+1, self.mflow_in[fl]))
            print("   hflow_in %d = %.3e J/kg" % (fl+1, self.hflow_in[fl]))
            for k, spec in enumerate(self.gas.species_names):
                if self.Yflow_in[fl][k] >= 1.e-20:
                    print("   Y%s_in %d = %.3e -" % (spec, fl+1,
                                                     self.Yflow_in[fl][k]))
        for k, spec in enumerate(self.gas.species_names):
            if self.gas.Y[k] >= 1.e-5:
                print("   Y" + spec + " = %.3e -" % self.gas.Y[k])
        return

    def compute_phi(self):
        '''Compute equivalence ratio.'''

        HCratio = (self.gas.elemental_mole_fraction('H')
                   /self.gas.elemental_mole_fraction('C'))
        FAR_st = (
            (self.gas.atomic_weight('C') + HCratio*self.gas.atomic_weight('H'))
            /((1. + HCratio/4.)*2.*self.gas.atomic_weight('O')))
        FAR = ((self.gas.elemental_mass_fraction('C')
                + self.gas.elemental_mass_fraction('H'))
               /self.gas.elemental_mass_fraction('O'))
        self.phi = FAR/FAR_st
        return

    def compute_laminar_flame_speed(self):
        '''
        Compute laminar flame speed from pre-tabulated array.
        WARNING: only valid for fresh gases!
        '''

        if self.h2table:
            Yfadd = (self.gas.Y[self.gas.species_index('H2')]
                     /(self.gas.Y[self.gas.species_index('C3H8')]
                       + self.gas.Y[self.gas.species_index('H2')]))
            SL = interpn((self.sl_pressList, self.sl_tempeList,
                          self.sl_phiList, self.sl_YfaddList), self.sl_table,
                         (self.gas.P, self.gas.T, self.phi, Yfadd),
                         'linear', bounds_error=False, fill_value=np.nan)[0]
        else:
            SL = interpn((self.sl_pressList, self.sl_tempeList,
                          self.sl_phiList), self.sl_table,
                         (self.gas.P, self.gas.T, self.phi),
                         'linear', bounds_error=False, fill_value=np.nan)[0]
        if SL == SL:
            self.SL = SL
        else:
            print(' WARNING: at %.2f b, %.2f K, phi %.2f ' % (self.gas.P*1.e-5,
                                                              self.gas.T,
                                                              self.phi))
            print('          laminar flame speed interpolation out of bounds \
                  or non computed value.')
            print('          -> the code use the last valid laminar flame \
                  speed used... (%.3f m/s)' % self.SL)
        return
