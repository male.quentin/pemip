"""
Duct class definition.
"""
import numpy as np


class duct:

    def __init__(self, nspec, nd):
        # temperature
        self.Tin = np.zeros(nd)
        self.Tout = np.zeros(nd)
        # speed
        self.Uin = np.zeros(nd)
        self.Uout = np.zeros(nd)
        # species
        self.Yin = np.zeros([nd, nspec])
        self.Yout = np.zeros([nd, nspec])
        # mflow
        self.mflow = np.zeros(nd)
        # geometry
        self.l = np.zeros(nd)
        self.d = np.zeros(nd)
        self.N = np.zeros(nd)
        self.S = np.zeros(nd)
        self.Stot = 0.
        self.dm = 0.
        # ignition
        self.TJI = np.zeros(nd)
