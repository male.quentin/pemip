# PEMIP

Pre-chamber Engine Model with Ignition Prediction (PEMIP), a multi-chamber, multi-zone engine model to predict the ignition of a lean main chamber by a pre-chamber [1]. It incorporates a jet ignition submodel based on Ref. [2].

[1] Quentin Malé et al. "Jet ignition prediction in a zero- dimensional pre-chamber engine model", International Journal of Engine Research, https://doi.org/10.1177/14680874211015002

[2] Quentin Malé et al. "Direct numerical simulations and models for hot burnt gases jet ignition", Combustion and Flame, https://doi.org/10.1016/j.combustflame.2020.09.017

## Description
In this pre-chamber engine model, the two chambers are connected by small cylindrical holes: the flame is ignited in the pre-chamber, hot gases propagate through the holes and ignite the main chamber through Turbulent Jet Ignition (TJI). The model original features are:
(i) separate balance equations for the pre- and main chambers, 
(ii) a specific model for temperature and com- position evolution in the holes and 
(iii) a DNS-based model to predict the ignition of the main chamber fresh gases by the burnt gases turbulent jets exiting the holes. 
Chemical reactions during TJI are the result of two competing mixing processes: (1) the hot jet gases mix with the fresh main chamber to produce heated zones and (2) at the same time, these hot gases cool down. (1) increases combustion and leads to ignition while (2) decreases it and can prevent ignition. 
The overall outcome (ignition or failure) is too complex to be modelled simply and the present model relies on recent DNSs of TJI which provided a method to predict the occurrence of ignition. Incorporating this DNS information into the engine model allows to predicts whether ignition will occur or not, an information which is not accessible otherwise using simple models. The resulting approach has been tested on multiple cases to predict ignition limits for very lean cases, effects of H2 injection into the pre-chamber and optimum size for the holes connecting the two chambers as a function of equivalence ratio [1].

## Installation
You can simply add the path toward PEMIP to your PYTHONPATH.
Cantera is required for thermochemistry computations.

## Usage
Use examples in the examples folder to get familiar with the code.

