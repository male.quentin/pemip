'''
 Engine definitions.
'''
import numpy as np


# Stroke
stroke = 90.e-3

# Conecting rod angle
conrod = 138.e-3

# Bore
bore = 76.e-3

# Volume at the top dead center
V_TDC = 27734.9286e-9

# Surface of the cylinder head walls
S_head = 0.00573378

# Surface of the piston walls
S_piston = 0.00464947

# Volume of the pre-chamber
V_pc = 1000.2917e-9

# Surface of the pre-chamber walls
S_pc = 0.0008679

# Diameter of the pre-chamber
d_pc = 6.0e-3

# Temperature of the walls
T_wall = 420.

# Length of the ducts
l_ducts = np.array([2.76e-3, 2.81e-3])

# diameter of the ducts
d_ducts = np.array([0.9e-3, 1.2e-3])

# number of ducts
n_ducts = np.array([2, 2])

# Temperature of the walls
Twall = 420.
