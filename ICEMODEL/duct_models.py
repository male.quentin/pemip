"""
[1] Quentin Malé et al. "Jet ignition prediction in a zero- dimensional
pre-chamber engine model", International Journal of Engine Research,
https://doi.org/10.1177/14680874211015002
"""
import sys
import numpy as np
import scipy.integrate
import cantera as ct


def Nusselt(Re, Pr, d, l, mu, mu_wall):
    '''
    Nusselt function (correlation).
    Sieder and Tate with correction to take into account the flow non fully
    developed.
    Sieder, E. N., & Tate, G. E. (1936). Heat Transfer and Pressure Drop of
    Liquids in Tubes. Industrial & Engineering Chemistry, 28(12), 1429–1435.
    http://doi.org/10.1021/ie50324a027
    correction ( 1 + d/l**(2/3) ) Heat Transmission, McAdams
    (recommendation for sudden contraction)
    '''
    # See Eq. (33) of Ref. [1]
    Nu = 0.027*Re**0.8*Pr**0.333333*(mu/mu_wall)**0.14*(1. + (d/l)**0.666666)
    return Nu


def get_duct_Tout_analytic(gas, Tin, P, Yin, mdot, Tw, d, l):
    '''
    Estimate the drop of temperature through the ducts.
    '''

    # get mu_wall
    gas.TPY = Tw, P, Yin
    mu_wall = gas.viscosity

    # --- thermo pties evaluated at Tin ---
    gas.TPY = Tin, P, Yin
    Re = 4.0 * mdot / np.pi / d / gas.viscosity
    Pr = gas.cp_mass * gas.viscosity / gas.thermal_conductivity
    Nu = Nusselt(Re, Pr, d, l, gas.viscosity, mu_wall)
    h = gas.thermal_conductivity * Nu / d

    # compute an estimation of the exhaust temperature, Eq. (32) of Ref. [1]
    Tout = Tw + (Tin - Tw)*np.exp(-l*np.pi*d*h/mdot/gas.cp_mass)

    return Tout


class ReactorOde(object):
    """Class of reactor that handles custom ODEs."""
    def __init__(self, gas, Twall, d, h):
        """Initialize the class."""
        # Parameters of the ODE system and auxiliary data
        self.gas = gas
        self.P = gas.P
        self.Twall = Twall
        self.d = d
        self.h = h

    def __call__(self, t, y):
        """The ODE function, y' = f(t,y)."""

        # State vector is [T, Y_1, Y_2, ... Y_K]
        self.gas.set_unnormalized_mass_fractions(y[1:])
        self.gas.TP = y[0], self.P
        rho = self.gas.density
        T = self.gas.T

        # h is evaluated once at the nozzle entry
        # first term RHS Eq. (30) of Ref. [1]
        Q = - 4.*self.h/self.d*(T - self.Twall)

        # chemical source terms
        wdot = self.gas.net_production_rates

        # RHS of dT/dt, Eq. (30) of Ref. [1]
        dTdt = Q - np.dot(self.gas.partial_molar_enthalpies, wdot)
        dTdt = dTdt/(rho*self.gas.cp)

        # RHS of dY/dt, Eq. (31) of Ref. [1]
        dYdt = wdot*self.gas.molecular_weights/rho

        return np.hstack((dTdt, dYdt))


def solve_ODEs(gas, Ti, Pi, Yi, mdot, Tw, d, l):
    '''Solve for Eqs (30) and (31) of Ref. [1].'''

    # get mu_wall
    gas.TPY = Tw, Pi, Yi
    mu_wall = gas.viscosity

    # gas initial ptes
    gas.TPY = Ti, Pi, Yi

    # evaluate h
    Re = 4.*mdot/np.pi/d/gas.viscosity
    Pr = gas.cp_mass*gas.viscosity/gas.thermal_conductivity
    Nu = Nusselt(Re, Pr, d, l, gas.viscosity, mu_wall)
    h = gas.thermal_conductivity*Nu/d

    # residence time
    tres = l*gas.density/mdot*np.pi*d**2/4.

    dt_max = tres/5.
    Kdt = 1.e-2

    y0 = np.hstack((gas.T, gas.Y))
    ode = ReactorOde(gas, Tw, d, h)
    solver = scipy.integrate.ode(ode)
    solver.set_integrator('vode', method='bdf', with_jacobian=True)
    solver.set_initial_value(y0, 0.0)

    # compute time step
    norm_prod_rate = np.sqrt(np.sum((gas.net_production_rates[:]
                                     *gas.molecular_weights[:])**2))
    dt = min(Kdt/norm_prod_rate, dt_max/10.)

    # loop until t=tres
    end = False
    while solver.successful() and not end:
        if (solver.t+dt) >= tres:
            dt = tres - solver.t
            end = True

        # integrate
        solver.integrate(solver.t + dt)

        # update
        gas.TPY = solver.y[0], Pi, solver.y[1:]

        # timestep calculation
        norm_prod_rate = np.sqrt(np.sum((gas.net_production_rates[:]
                                         *gas.molecular_weights[:])**2))
        dt = min(Kdt/norm_prod_rate, dt_max)

    if not end:
        sys.exit('ERROR DUCT ODEs SOLVER')
    else:
        To = solver.y[0]
        Yo = solver.y[1:]

    return To, Yo


def get_duct_flow_type(obj):
    '''Return the type of mixture that flows through the ducts.'''

    if obj.mc.gas.P >= obj.pc.gas.P:
        # from mc -> pc
        if obj.mc.comb_started:
            # reverse flow because of mc combustion, assumed to be burnt
            flowType = 'burnt'
            assert obj.mcb.mass > 0.0
        else:
            # flow because of mc piston compression, assumed to be fresh
            flowType = 'fresh'
            assert obj.mcf.mass > 0.0
    else:
        # from pc -> mc
        if obj.mc.burnt_gases_injection_started:
            # if threshold is passed, burnt gases
            flowType = 'burnt'
            assert obj.pcb.mass > 0.0
        else:
            # if not, fresh gases
            flowType = 'fresh'
            assert obj.pcf.mass > 0.0

    return flowType


def get_Cd(rhoin, muin, Uin, d, l):
    '''
    Compute the discharge coefficient, Eq. (27) of Ref. [1].
    ASHIMIN V.I., GELLER Z. I. and SKOBEL'CIN Y.A, “Discharge coefficients of
    a real fluid from cylindrical orifices” (in Russian), Oil. Ind., Moscou,
    IX-1961.
    '''
    Re = rhoin*Uin*d/muin
    Cd = (1.23 + 58./max(Re, 500.0)*l/d)**(-1)
    return Cd


def get_mflow(Cd, S, X, gamma_up, W_up, T_up, P_up):
    '''Compute mass flow.'''

    R = ct.gas_constant*1e-3

    mflow = (Cd*S*P_up
             *np.sqrt(2.*gamma_up*W_up/(gamma_up-1.)/R/T_up)
             *np.sqrt(X**(-2./gamma_up)-X**(-(gamma_up+1.)/gamma_up)))

    return mflow


def set_zone_duct_flow(zin, zout, gbl_zin, gbl_zout, duct,
                       gas=None, Tw=None, duct_model='None',
                       force_equil=False):
    '''Compute duct flow and append flow characteristics to the zones.'''

    # critical pressure ratio
    gup = zin.gas.cp_mass/zin.gas.cv_mass
    XC = ((gup + 1.)/2.)**(gup/(gup - 1.))
    X = min(zin.gas.P/zout.gas.P, XC)

    # isentropic expansion
    P = zin.gas.P/X
    Tin = zin.gas.T*X**((1. - gup)/gup)
    rhoin = zin.gas.density*X**(-1./gup)

    for i in range(len(duct.d)):

        # surface of the i^th ducts
        S = duct.N[i]*duct.S[i]

        # mass flow
        Cd = get_Cd(rhoin=zin.gas.density, muin=zin.gas.viscosity,
                    Uin=duct.Uin[i], d=duct.d[i], l=duct.l[i])
        mflow = get_mflow(Cd=Cd, S=S, X=X, gamma_up=gup,
                          W_up=zin.gas.mean_molecular_weight*1e-3,
                          T_up=zin.gas.T, P_up=zin.gas.P)
        zin.mflow_out.append(mflow)
        gbl_zin.mflow_out.append(mflow)
        zout.mflow_in.append(mflow)
        gbl_zout.mflow_in.append(mflow)

        if (duct_model == 'analytic'
            or (duct_model == 'auto' and duct.TJI[i] == 1)):
            # heat losses
            Tout = get_duct_Tout_analytic(
                gas, Tin=Tin, P=P, Yin=zin.gas.Y,
                mdot=mflow/float(duct.N[i]), Tw=Tw, d=duct.d[i],
                l=duct.l[i])

            # mixture composition
            gas.TPY = Tout, P, zin.gas.Y
            if force_equil:
                gas.equilibrate('TP')
            Yout = gas.Y
            hout = gas.enthalpy_mass
            rhoout = gas.density

        elif (duct_model == 'ODEs'
              or (duct_model == 'auto' and duct.TJI[i] == 0)):
            Tout, Yout = solve_ODEs(gas, Ti=Tin, Pi=P, Yi=zin.gas.Y,
                                    mdot=mflow/float(duct.N[i]), Tw=Tw,
                                    d=duct.d[i], l=duct.l[i])

            # mixture composition
            gas.TPY = Tout, P, Yout
            if force_equil:
                gas.equilibrate('TP')
            Yout = gas.Y
            hout = gas.enthalpy_mass
            rhoout = gas.density

        else:
            # no heat losses
            Tout = Tin

            # mixture composition
            gas.TPY = Tout, P, zin.gas.Y
            if force_equil:
                gas.equilibrate('TP')
            Yout = gas.Y
            hout = gas.enthalpy_mass
            rhoout = gas.density

        # set zone ptes
        zout.hflow_in.append(hout)
        gbl_zout.hflow_in.append(hout)
        zout.Yflow_in.append(Yout)
        gbl_zout.Yflow_in.append(Yout)

        # set duct ptes
        duct.Uin[i] = mflow/rhoin/S
        duct.Uout[i] = mflow/rhoout/S
        duct.Tin[i] = Tin
        duct.Tout[i] = Tout
        duct.Yin[i, :] = zin.gas.Y
        duct.Yout[i, :] = Yout
        duct.mflow[i] = mflow

    return zin, zout, gbl_zin, gbl_zout, duct
