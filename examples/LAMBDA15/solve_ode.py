'''Example'''
import pickle
import os
import numpy as np
import cantera as ct
import time

import ICEMODEL.engine_defs as engineDefs
from ICEMODEL.ode_defs import EngineOde
from ICEMODEL.useful_functions import compute_initial_yvect, init_states, \
                                      update_states, crop_states
import operating_point as opDefs


ct.suppress_thermo_warnings()

# -> settings
# output directory
outdir = 'OUTPUT'
# end CAD
end_CAD = 60.
# save every X CAD
save_every = 0.25
# size of states arrays
states_length = int((end_CAD-opDefs.RFA)/save_every) + 1
# cantera cti file
cti = 'reducedS37R129_0.cti'
# cantera gas object
gas = ct.Solution(cti)
# flame speed table
sl_table = 'flame_speed_table.h5'

# -> initialize the object
ode = EngineOde(cti, engineDefs, opDefs, sl_table)

# -> init dict of states
states = init_states(gas.species_names, states_length, len(engineDefs.d_ducts))

# -> set initial values
t = 0.
y = compute_initial_yvect(gas, cti, opDefs, engineDefs, ode, sl_table)

# -> advance in time using 1 step euler
print('{:>6} {:>7} {:>6} {:>6} {:>7} {:>7} {:>7} {:>7} {:>7} {:>7}'.format(
    'it', 'CAD', 'Pmc b', 'Ppc b', 'Tmc K', 'Tpc K', 'bfpc', 'bfmc', 'FlSfpc',
    'FlSfmc'))
nit = 0
its = 0
end = False
CADsave = opDefs.RFA
tic = time.perf_counter()
while not end:
    # -> evalute dydt = f(t, y) and update ode zone variables with current t, y
    try:
        dydt = ode(t, y)
    except Exception as e:
        sys.exit(e)

    # -> output
    if ode.CAD >= CADsave:
        CADsave += save_every
        # update the state dict
        states = update_states(states, ode.mc, ode.mcf, ode.mcb, ode.pc,
                               ode.pcf, ode.pcb, ode.duct, ode.CAD, its)
        its += 1
        # print current state
        print('{:6d} {:7.2f} {:6.2f} {:6.2f} {:7.2f} {:7.2f} {:4.5f} {:4.5f}\
               {:6.1e} {:6.1e}'.format(
                   nit, ode.CAD, ode.mc.gas.P*1e-5, ode.pc.gas.P*1e-5,
                   ode.mc.gas.T, ode.pc.gas.T, ode.pcb.mass/ode.pc.mass,
                   ode.mcb.mass/ode.mc.mass, ode.pc.FlamSurf, ode.mc.FlamSurf))

    # -> evaluate the time step to advance in time
    dt = 1000.0*np.amin(np.divide(1.0, np.clip(np.abs(dydt), a_min=1.0e-120,
                                               a_max=None)))
    dt = min(dt, save_every/(opDefs.rpm*360.0/60.0))

    if ode.CAD > end_CAD:
        # -> terminate
        end = True
    elif (ode.pc.comb_ended and (not ode.mc.comb_started)
          and np.amax(ode.duct.Uout[:]) < ode.Ujet_thr):
        end = True
    elif ode.pc.comb_ended and ode.mc.comb_ended:
        end = True
    else:
        # -> advance in time
        t = t + dt
        y = y + dt*dydt
        nit += 1

states = crop_states(states, gas.species_names, its)

toc = time.perf_counter()
print(f"Solved in {toc - tic:0.4f} seconds\n")

# export dict of data
states['BGE_START'] = ode.mc_burnt_gases_injection_start
states['BGE_END'] = ode.mc_burnt_gases_injection_end
states['PC_COMB_START'] = ode.pc_comb_start
states['PC_COMB_END'] = ode.pc_comb_end
states['MC_COMB_START'] = ode.mc_comb_start
states['MC_COMB_END'] = ode.mc_comb_end
print('Export data to pickle...')
if not os.path.exists(outdir):
    os.makedirs(outdir)
f = open("%s/states.pkl" % outdir, "wb")
pickle.dump(states, f)
f.close()

print('\nAll done!')
