"""
Model for h heat losses coef for the walls.
[1] Quentin Malé et al. "Jet ignition prediction in a zero- dimensional
pre-chamber engine model", International Journal of Engine Research,
https://doi.org/10.1177/14680874211015002
"""


def get_hwall(L, N, stroke, nu, lbda, a):
    """
    The heat transfer coefficient is estimated using a specific correlation for
    four-stroke engines.
    W. J. D. Annand
    """

    # mean piston speed
    U = 2.*N/60.*stroke

    # Eq. (16) of Ref. [1]
    hwall = lbda/L*a*(U*L/nu)**0.7

    return hwall
